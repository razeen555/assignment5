//
//  main.c
//  crafting
//
//  Created by Razeen on 6/21/20.
//  Copyright © 2020 Razeen. All rights reserved.
//
int viewers (int value){
    const int viewers =120;
    const int price =15;
    return viewers - ((value-price)/5*20);
}

//Calculating the Cost
int cost (int value){
    const int def_cost = 500;
    return def_cost + viewers(value)*3;
}

int income (int value){
    return viewers(value)*value;
}

int profit (int value){
    return income(value)- cost(value);
}



int main(){
    
    printf("relationship between the Price and the excepted Profit =\n");
    int price, final_profit, maximum_profit, maximum_price;
    maximum_profit = 0;
    for (price=5; price<=40; price+=5){
        final_profit = profit(price);
        printf("\nPrice: %d  Profit: %d\n", price,final_profit );
        if(maximum_profit < final_profit)
        {
            maximum_profit = final_profit;
            maximum_price = price;
        }
    }
    
    printf("finally the suitable Maximum profit is Rs.%d and The matching ticket price is Rs. %d\n", maximum_profit, maximum_price);
    return 0;
}
